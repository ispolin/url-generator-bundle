### UrlGenerator
It is a component for generation all possible urls for the application based on
configuration to keep this data and post process it for different purposes like sitemap
generation, http cache warm up or some analysis. 

For achive this goal UrlGenerator needs next components to be properly configured.

It is `PossibleOptionProvider` (based on source configuration) and
`ParameterSetProvider`

Let me explain on example of typical route for ecommerce shops.

The route

    /category/{alias}/shop/{shopAlias}


And controller action:

    public function searchByShopAction(
        Request $request,
        ProductCategory $category,
        Shop $shop,
    ): Response {

And configuration:

    {
        "sources" : {
            {
                   {
                       "route_parameter_name": "alias",
                        "action_parameter":"category",
                        "method":"repository.getByParamForAll('alias')"
                   },
                   {
                        "route_parameter_name": "shopAlias",
                        "action_parameter":"shop",
                        "method":"repository.getByParamForAll('alias')"
                   }
            }
        }
    }
     

#### Soruces list
`Soruces` descirbes the way where we will get parameters for this route (`PossibleOptionProvider`). 
As you can see we have 2 sources, one for `alias` and one for `shopAlias`.

`"route_parameter_name": "alias"` - allows to get additional information from php function description itself.
`"action_parameter":"category"` - describing the route parameter where to apply this data
`"method":"repository.getByParamForAll('alias')"` expr. for the method (of entity repository in this case) which will return
data. 

In few steps we can say:
1) Get EntityClass from controller action paramter `alias`
2) Call expr `repository.getByParamForAll('alias')`
3) Prvide result as for `category` route parameter


`PossibleOptionProvider` will be initalized based on each of source.


### PossibleOptionProvider 
Need for getting all possible values of one particular parameter in the route. 

    
 PossibleOptionProvider need to provide all possible values of parameters {category_alias} and
 {shop_alias}. Now we support only `EntityRepository` to execute some method which will 
 return all possible options. 
  
### ParameterSetProvider

Need for getting all possible parameters sets for future url generation

For example 

`category_alias` can be `womens-clothing` and `mans-clothing` 
`shop_alias` can be `lamoda` or `adidas`

in case of  `BruteForceCombinator` the result will be

```json
[
    {
        "category_alias":"womens-clothing",
        "shop_alias":"lamoda"
    },
    {
        "category_alias":"mans-clothing",
        "shop_alias":"lamoda"
    },
    {
        "category_alias":"womens-clothing",
        "shop_alias":"adidas"
    },
    {
        "category_alias":"mans-clothing",
        "shop_alias":"adidas"
    }
]
```
