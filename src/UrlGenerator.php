<?php

namespace Ispolin\UrlGeneratorBundle;

use Exception;
use Ispolin\UrlGeneratorBundle\Configuration\ParameterSetProviderConfiguration;
use Ispolin\UrlGeneratorBundle\Configuration\UrlGeneratorConfiguration;
use Ispolin\UrlGeneratorBundle\Configuration\ValueProviderConfiguration;
use Ispolin\UrlGeneratorBundle\DTO\GeneratedUrl;
use Ispolin\UrlGeneratorBundle\ParameterSetProvider\ParameterSetsProviderInterface;
use Ispolin\UrlGeneratorBundle\PossibleOptionProvider\PossibleOptionProviderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UrlGenerator
{
    /** @var PossibleOptionProviderInterface[] */
    protected $possibleOptionGenerators;

    /** @var ParameterSetsProviderInterface[] */
    protected $parameterSetProviders;

    /** @var UrlGeneratorInterface */
    protected $urlGenerator;

    /** @var array */
    protected $possibleValues;

    /** @var UrlGeneratorConfiguration */
    protected $config;

    /**
     * UrlGenerator constructor.
     *
     * @param PossibleOptionProviderInterface[] $possibleOptionProviders
     * @param ParameterSetsProviderInterface[]  $parameterSetProviders
     * @param UrlGeneratorInterface             $generator
     */
    public function __construct(
        iterable $possibleOptionProviders,
        iterable $parameterSetProviders,
        UrlGeneratorInterface $generator
    ) {
        $this->possibleOptionGenerators = $possibleOptionProviders;
        $this->parameterSetProviders = $parameterSetProviders;
        $this->urlGenerator = $generator;
    }

    public function setInitialState(UrlGeneratorConfiguration $config): void
    {
        $this->config = $config;
        $this->possibleValues = [];
    }

    private function generateParameterSets(ParameterSetProviderConfiguration $configuration): array
    {
        $found = false;
        foreach ($this->parameterSetProviders as $provider) {
            if ($configuration->getClass() === get_class($provider)) {
                $found = true;
                break;
            }
        }

        if (!$found) {
            throw new Exception('Unable to generate possible parameters set for '.json_encode($configuration));
        }

        return $provider->generate($this->possibleValues);
    }

    /**
     * @return GeneratedUrl[]
     *
     * @throws Exception
     */
    public function generate(): iterable
    {
        // Load all possible options
        foreach ($this->config->getValueProviders() as $providerConfiguration) {
            $this->generatePossibleValues($providerConfiguration);
        }

        // Process all sets
        foreach ($this->config->getParameterSetProviders() as $providerConfiguration) {
            $sets = $this->generateParameterSets($providerConfiguration);

            foreach ($providerConfiguration->getRoutes() as $routeName) {
                foreach ($sets as $set) {
                    yield new GeneratedUrl($this->urlGenerator->generate($routeName, $set), $routeName);
                }
            }
        }
    }

    private function generatePossibleValues(ValueProviderConfiguration $configuration): void
    {
        $found = false;
        foreach ($this->possibleOptionGenerators as $generator) {
            if ($configuration->getClass() === get_class($generator)) {
                $found = true;
                break;
            }
        }

        if (!$found) {
            throw new Exception('Unable to generate possible options for '.json_encode($configuration));
        }

        $data = $generator->generate($configuration);

        // Mapping
        if ($configuration->getRouteParameter()) {
            $this->possibleValues[$configuration->getRouteParameter()] = $data;
        } else {
            foreach ($data as $d) {
                foreach ($d as $k => $v) {
                    $this->possibleValues[$k][] = $v;
                }
            }
        }

        $data = null;
    }
}
