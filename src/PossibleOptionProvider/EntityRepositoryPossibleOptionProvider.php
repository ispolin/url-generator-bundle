<?php

namespace Ispolin\UrlGeneratorBundle\PossibleOptionProvider;

use Doctrine\Common\Persistence\ManagerRegistry;
use Ispolin\UrlGeneratorBundle\Configuration\ValueProviderConfiguration;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

class EntityRepositoryPossibleOptionProvider implements PossibleOptionProviderInterface
{
    /**
     * @var ManagerRegistry
     */
    private $registry;

    /**
     * @var ExpressionLanguage
     */
    private $expressionLanguage;

    /**
     * EntityRepositoryOptionGenerator constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
        $this->expressionLanguage = new ExpressionLanguage();
    }

    public function generate(ValueProviderConfiguration $configuration): iterable
    {
        $repo = $this->registry->getRepository($configuration->getOptions()['entity_class']);

        return $this->expressionLanguage->evaluate('repository.'.$configuration->getMethod(), ['repository' => $repo]);
    }
}
