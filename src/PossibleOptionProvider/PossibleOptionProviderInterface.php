<?php

namespace Ispolin\UrlGeneratorBundle\PossibleOptionProvider;

use Ispolin\UrlGeneratorBundle\Configuration\ValueProviderConfiguration;

interface PossibleOptionProviderInterface
{
    public function generate(ValueProviderConfiguration $configuration): iterable;
}
