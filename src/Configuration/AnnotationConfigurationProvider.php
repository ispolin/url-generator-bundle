<?php

namespace Ispolin\UrlGeneratorBundle\Configuration;

use Doctrine\Common\Annotations\Reader;
use Ispolin\UrlGeneratorBundle\Annotation\UrlGenerator;
use ReflectionMethod;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouterInterface;

class AnnotationConfigurationProvider
{
    /** @var RouterInterface */
    private $router;

    /**
     * @var Reader
     */
    private $annotationReader;

    /**
     * AnnotationConfigurationProvider constructor.
     */
    public function __construct(
        RouterInterface $router,
        Reader $annotationReader
    ) {
        $this->router = $router;
        $this->annotationReader = $annotationReader;
    }

    /**
     * @return UrlGeneratorConfiguration[]
     */
    public function getUrlGenerators(): iterable
    {
        $urlGenerators = $processedActions = [];

        foreach ($this->router->getRouteCollection() as $routeName => $route) {
            $key = $route->getDefault('_controller');
            $routes[$key][] = $route;

            if (isset($processedActions[$key])) {
                continue;
            }

            if ($annotation = $this->getUrlGeneratorAnnotation($route)) {
                $urlGenerators[$key] = UrlGeneratorConfiguration::fromAnnotation($annotation);
            }

            $processedActions[$key] = true;
        }

        return $urlGenerators;
    }

    private function getUrlGeneratorAnnotation(Route $route): ?UrlGenerator
    {
        $f = $route->getDefault('_controller');
        $f = explode('::', $f);

        if (!class_exists($f[0])) {
            return null;
        }

        if (!isset($f[1])) {
            return null;
        }

//        $c = new \ReflectionClass($f[0]);
//        $method = $c->getMethod($f[1]);
        //dump($method->getParameters());

        foreach ($this->annotationReader->getMethodAnnotations(new ReflectionMethod($f[0], $f[1])) as $annotation) {
            if (UrlGenerator::class === get_class($annotation)) {
                return $annotation;

                break;
            }
        }

        return null;
    }
}
