<?php

namespace Ispolin\UrlGeneratorBundle\Configuration;

use Ispolin\UrlGeneratorBundle\Annotation\ParameterSetProviderInterface;

class ParameterSetProviderConfiguration
{
    /** @var array */
    private $routes;

    /** @var string */
    private $class;

    // TODO
    public static function fromArray(array $a): ParameterSetProviderConfiguration
    {
    }

    public static function fromAnnotation(ParameterSetProviderInterface $annotation): ParameterSetProviderConfiguration
    {
        $res = new self();
        $res->setRoutes($annotation->routes);
        $res->setClass($annotation->class);

        return $res;
    }

    public function getRoutes(): array
    {
        return $this->routes;
    }

    public function setRoutes(array $routes): void
    {
        $this->routes = $routes;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass(string $class): void
    {
        $this->class = $class;
    }
}
