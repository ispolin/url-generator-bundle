<?php

namespace Ispolin\UrlGeneratorBundle\Configuration;

use Ispolin\UrlGeneratorBundle\Annotation\UrlGenerator;

class UrlGeneratorConfiguration
{
    /** @var ValueProviderConfiguration[] */
    private $valueProviders;

    /** @var ParameterSetProviderConfiguration[] */
    private $parameterSetProviders;

    public static function fromArray(array $a): UrlGeneratorConfiguration
    {
    }

    public static function fromAnnotation(UrlGenerator $annotation): UrlGeneratorConfiguration
    {
        $res = new self();

        $valueProviders = [];
        foreach ($annotation->valueProviders as $provider) {
            $valueProviders[] = ValueProviderConfiguration::fromAnnotation($provider);
        }
        $res->setValueProviders($valueProviders);

        $parameterSetProviders = [];
        foreach ($annotation->parameterSets as $provider) {
            $parameterSetProviders[] = ParameterSetProviderConfiguration::fromAnnotation($provider);
        }

        $res->setParameterSetProviders($parameterSetProviders);

        return $res;
    }

    /**
     * @return ParameterSetProviderConfiguration[]
     */
    public function getParameterSetProviders(): array
    {
        return $this->parameterSetProviders;
    }

    /**
     * @param ParameterSetProviderConfiguration[] $parameterSetProviders
     */
    public function setParameterSetProviders(array $parameterSetProviders): void
    {
        $this->parameterSetProviders = $parameterSetProviders;
    }

    /**
     * @param ValueProviderConfiguration[] $valueProviders
     */
    public function setValueProviders(array $valueProviders): void
    {
        $this->valueProviders = $valueProviders;
    }

    /**
     * @return ValueProviderConfiguration[]
     */
    public function getValueProviders(): array
    {
        return $this->valueProviders;
    }
}
