<?php

namespace Ispolin\UrlGeneratorBundle\Configuration;

use Ispolin\UrlGeneratorBundle\Annotation\ValueProviderInterface;

class ValueProviderConfiguration
{
    /** @var null|string */
    protected $routeParameter;

    /** @var array */
    protected $options;

    /** @var string */
    protected $method;

    /** @var string */
    protected $class;

    // TODO
    public static function fromArray()
    {
    }

    public static function fromAnnotation(ValueProviderInterface $annotation): ValueProviderConfiguration
    {
        $res = new self();

        $res->routeParameter = $annotation->routeParameter;
        $res->method = $annotation->method;
        $res->class = $annotation->class;
        $res->options = $annotation->getOptions();

        return $res;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getRouteParameter(): ?string
    {
        return $this->routeParameter;
    }

    public function getClass(): string
    {
        return $this->class;
    }
}
