<?php

namespace Ispolin\UrlGeneratorBundle\DTO;

class GeneratedUrl
{
    /** @var string */
    protected $url;

    /** @var string */
    protected $routeName;

    /**
     * GeneratedUrl constructor.
     */
    public function __construct(string $url, string $routeName)
    {
        $this->url = $url;
        $this->routeName = $routeName;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getRouteName(): string
    {
        return $this->routeName;
    }
}
