<?php

namespace Ispolin\UrlGeneratorBundle\Annotation;

interface ValueProviderInterface
{
    public function getOptions(): array;
}
