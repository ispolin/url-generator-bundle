<?php

namespace Ispolin\UrlGeneratorBundle\Annotation\ValueProvider;

use Ispolin\UrlGeneratorBundle\Annotation\ValueProviderInterface;
use Ispolin\UrlGeneratorBundle\PossibleOptionProvider\EntityRepositoryPossibleOptionProvider;

/**
 * @Annotation
 */
class EntityRepository implements ValueProviderInterface
{
    /** @var string */
    public $routeParameter;

    /** @var string */
    public $entityClass;

    /** @var string */
    public $method;

    /** @var string */
    public $class = EntityRepositoryPossibleOptionProvider::class;

    public function getOptions(): array
    {
        return [
            'entity_class' => $this->entityClass,
        ];
    }
}
