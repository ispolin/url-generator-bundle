<?php

namespace Ispolin\UrlGeneratorBundle\Annotation;

use Exception;
use Ispolin\UrlGeneratorBundle\ParameterSetProvider\Configuration;
use Ispolin\UrlGeneratorBundle\ParameterSetProvider\ParameterSetsProviderInterface;
use Ispolin\UrlGeneratorBundle\PossibleOptionProvider\PossibleOptionProviderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

interface ParameterSetProviderInterface
{
}
