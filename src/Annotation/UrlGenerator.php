<?php

namespace Ispolin\UrlGeneratorBundle\Annotation;

/**
 * @Annotation
 */
class UrlGenerator
{
    /** @var Ispolin\UrlGeneratorBundle\Annotation\ParameterSetProviderInterface[] */
    public $parameterSets;

    /** @var Ispolin\UrlGeneratorBundle\Annotation\ValueProviderInterface[] */
    public $valueProviders;
}
