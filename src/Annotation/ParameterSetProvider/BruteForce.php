<?php

namespace Ispolin\UrlGeneratorBundle\Annotation\ParameterSetProvider;

use Ispolin\UrlGeneratorBundle\Annotation\ParameterSetProviderInterface;
use Ispolin\UrlGeneratorBundle\ParameterSetProvider\BruteForceCombinatorParameterSetProvider;

/**
 * @Annotation
 */
class BruteForce implements ParameterSetProviderInterface
{
    /** @var array */
    public $routes;

    /** @var string */
    public $class = BruteForceCombinatorParameterSetProvider::class;
}
