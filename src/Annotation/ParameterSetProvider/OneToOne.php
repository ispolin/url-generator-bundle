<?php

namespace Ispolin\UrlGeneratorBundle\Annotation\ParameterSetProvider;

use Ispolin\UrlGeneratorBundle\Annotation\ParameterSetProviderInterface;
use Ispolin\UrlGeneratorBundle\ParameterSetProvider\OneToOneParameterSetProvider;

/**
 * @Annotation
 */
class OneToOne implements ParameterSetProviderInterface
{
    /** @var array */
    public $routes;

    /** @var string */
    public $class = OneToOneParameterSetProvider::class;
}
