<?php

namespace Ispolin\UrlGeneratorBundle\ParameterSetProvider;

use Exception;

class OneToOneParameterSetProvider implements ParameterSetsProviderInterface
{
    public function generate(iterable &$possibleOptions): iterable
    {
        $res = [];

        $parameters = array_keys($possibleOptions);
        $count = null;
        foreach ($parameters as $key) {
            if (null === $count) {
                $count = count($possibleOptions[$key]);
            } elseif ($count !== count($possibleOptions[$key])) {
                throw new Exception('Unable to combine. Sets has different counts');
            }
        }

        for ($i = 0; $i < $count; ++$i) {
            foreach ($parameters as $key) {
                $res[$i][$key] = $possibleOptions[$key][$i];
//                unset($possibleOptions[$key][$i]) = null;
            }
        }

        return $res;
    }
}
