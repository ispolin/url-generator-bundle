<?php

namespace Ispolin\UrlGeneratorBundle\ParameterSetProvider;

class BruteForceCombinatorParameterSetProvider implements ParameterSetsProviderInterface
{
    public function generate(iterable &$possibleOptions): iterable
    {
        if (1 === count($possibleOptions)) {
            $key = array_keys($possibleOptions)[0];

            $res = [];
            foreach ($possibleOptions[$key] as $r) {
                $res[] = [$key => $r];
            }

            return $res;
        }

        $kff = array_keys($possibleOptions);

        $r = $this->combinations(array_values(
            $possibleOptions
        ));

        foreach ($r as $k => $v) {
            $withKeys = [];

            foreach ($kff as $idx => $key) {
                $withKeys[$key] = $v[$idx];
            }
            $r[$k] = $withKeys;
        }

        return $r;
    }

    private function combinations($arrays, $i = 0)
    {
        if (!isset($arrays[$i])) {
            return [];
        }
        if ($i === count($arrays) - 1) {
            return $arrays[$i];
        }

        // get combinations from subsequent arrays
        $tmp = $this->combinations($arrays, $i + 1);

        $result = [];

        // concat each array from tmp with each element from $arrays[$i]
        foreach ($arrays[$i] as $v) {
            foreach ($tmp as $t) {
                $result[] = is_array($t) ?
                    array_merge([$v], $t) :
                    [$v, $t];
            }
        }

        return $result;
    }
}
