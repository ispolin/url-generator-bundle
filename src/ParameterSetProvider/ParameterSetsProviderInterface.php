<?php

namespace Ispolin\UrlGeneratorBundle\ParameterSetProvider;

interface ParameterSetsProviderInterface
{
    public function generate(iterable &$params): iterable;
}
